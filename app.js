const express = require('express'),
server  = require('./book/server'), 
config = require('./config');;
var app = express();
var bookApp = new server(config);
app.get('/',function(req,res){
    bookApp.API(req,res);
});
app.get('/source',function(req,res){
    bookApp.SOURCE(req,res);
});
app.get('/hot',function(req,res){
    bookApp.HOT(req,res);
});
app.get('/search',function(req,res){
    bookApp.SEARCH(req,res);
});
app.get('/book',function(req,res){
    bookApp.BOOK(req,res);
});
app.get('/book/read/:key/*',function(req,res){
    bookApp.BOOKREADINFO(req,res);
});
app.get('/book/:key/*',function(req,res){
    bookApp.BOOKREAD(req,res);
});
app.get('/public/:key/*',function(req,res){
    bookApp.PUBLIC(req,res);
});

app.listen(2025);